({
	init : function(component, event, helper) {
		helper.init(component, event);
	},

	handleTransfer : function(component, event, helper) {
	    helper.init(component, event);
    },

    editRecord : function(component, event, helper) {
        helper.editRecord(component, event)
    },

    returnToQueue : function(component, event, helper) {
        helper.returnToQueue(component, event);
    },

    openTransferCaseModal : function(component, event, helper) {
        helper.openTransferCaseModal(component, event);
    },

    accept : function(component, event, helper) {
        helper.checkOwnership(component, event, helper)
        .then(function() {
            return helper.accept(component, event, helper);
        })
        .catch(function(error) {
            component.set('v.errorMessage', error);
        });
    },

    refresh : function(component, event, helper) {
        helper.init(component, event);
    },

    updateCSS : function(component, event, helper) {
        console.log('updateCSS');
        var owner = component.get("v.whoOwnsRecord");
        console.log('owner: ', owner);
        if(owner == 'user') {
            var header = component.find("header");
            $A.util.addClass(header, "greyBackground");
            $A.util.removeClass(header, "redBackground");
            $A.util.removeClass(header, "purpleBackground");
            var acceptButton = component.find("acceptCase");
            $A.util.addClass(acceptButton, "hide");
            var editRecordButton = component.find("editRecord");
            $A.util.removeClass(editRecordButton, "hide");
            var transferButton = component.find("transfer");
            $A.util.removeClass(transferButton, "hide");
            var returnToQueueButton = component.find("returnToQueue");
            $A.util.removeClass(returnToQueueButton, "hide");
        }
        else if(owner == 'otherUser') {
            var header = component.find("header");
            $A.util.removeClass(header, "greyBackground");
            $A.util.addClass(header, "redBackground");
            $A.util.removeClass(header, "purpleBackground");
            var acceptButton = component.find("acceptCase");
            $A.util.addClass(acceptButton, "hide");
            var returnToQueueButton = component.find("returnToQueue");
            $A.util.addClass(returnToQueueButton, "hide");
            var editRecordButton = component.find("editRecord");
            $A.util.addClass(editRecordButton, "hide");
            var transferButton = component.find("transfer");
            $A.util.addClass(transferButton, "hide");
        }
        else {
            var header = component.find("header");
            $A.util.removeClass(header, "greyBackground");
            $A.util.removeClass(header, "redBackground");
            $A.util.addClass(header, "purpleBackground");
            var acceptButton = component.find("acceptCase");
            $A.util.removeClass(acceptButton, "hide");
            var returnToQueueButton = component.find("returnToQueue");
            $A.util.addClass(returnToQueueButton, "hide");
            var editRecordButton = component.find("editRecord");
            $A.util.removeClass(editRecordButton, "hide");
            var transferButton = component.find("transfer");
            $A.util.removeClass(transferButton, "hide");
        }
    }
})