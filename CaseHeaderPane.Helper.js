({
    init : function(component, event) {
        console.log('init');
        var sObject = {};
        component.set("v.sObject", sObject);
        var action = component.get('c.getFields');
        action.setParams({
            sObjectName: component.get('v.sObjectName'),
            fieldSetName: component.get('v.fieldSetName'),
            id: component.get('v.recordId')

        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var fields = response.getReturnValue();
                component.set("v.fields", fields);
                for(var i = 0; i < fields.length; i++) {
                    if(fields[i].fieldPath == 'OwnerId') {
                        console.log('ownerId: ', fields[i].value);
                        if(fields[i].value == $A.get("$SObjectType.CurrentUser.Id")) {
                            component.set("v.whoOwnsRecord", 'user');
                        }
                        else if(fields[i].value.substring(0, 3) == '00G') {
                            console.log('setting owner to queue');
                            component.set("v.whoOwnsRecord", 'queue');
                        }
                        else {
                            component.set("v.whoOwnsRecord", 'otherUser');
                        }

                    }
                    if(fields[i].fieldPath == 'Subject') {
                        component.set("v.subject", fields[i].value);
                    }
                    else if(fields[i].fieldPath == 'CustomerType__c') {
                        component.set("v.customerType", fields[i].value);
                    }
                    else if(fields[i].fieldPath == 'CustomerTypeBannedSuspended__c') {
                        component.set("v.customerTypeBannedSuspended", fields[i].value);
                    }
                    else if(fields[i].fieldPath == 'Labels__c') {
                        component.set("v.labels", fields[i].value);
                    }
                    else {

                    }
                }

                //$A.get('e.force:refreshView').fire();
            }
            else {
                window.scrollTo(0,0);
                helper.showError(component, event, helper, $A.get('$Label.c.cs_get_fields_error'));
            }
        });

        $A.enqueueAction(action);
    },

    editRecord : function(component, event) {
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": component.get("v.recordId")
        });
        editRecordEvent.fire();
    },

    returnToQueue : function(component, event) {
        var action = component.get('c.returnCaseToQueue');
        var recIds = [];
        recIds.push(component.get("v.recordId"));
        action.setParams({
            caseIds : recIds
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var workspaceAPI = component.find("workspace");
                workspaceAPI.getFocusedTabInfo().then(function(response) {
                    var focusedTabId = response.tabId;
                    workspaceAPI.closeTab({tabId: focusedTabId});
                })
                .catch(function(error) {
                    window.scrollTo(0,0);
                    helper.showError(component, event, helper, $A.get('$Label.c.cs_open_tabs_Error'));
                });
            }
            else {
                window.scrollTo(0,0);
                helper.showError(component, event, helper, $A.get('$Label.c.cs_return_case_to_queue_error'));
            }
        });

        $A.enqueueAction(action);
    },

    closeFocusedTab : function(component, event) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo().then(function(response) {
            var focusedTabId = response.tabId;
            workspaceAPI.closeTab({tabId: focusedTabId});
        })
        .catch(function(error) {
            console.log(error);
        });
    },

    openTransferCaseModal : function(component, event) {
        var self = this;
        $A.createComponent('c:TransferCases', {
            selectedCases : [{'Id' : component.get('v.recordId')}]
        }, function(newComponent, status, error) {
            if (status === 'SUCCESS') {
                var placeholder = component.find('modalPlaceholder');
                var body = placeholder.get('v.body');
                body.push(newComponent);
                placeholder.set('v.body', body);
            }
            else {
                window.scrollTo(0,0);
                helper.showError(component, event, helper, $A.get('$Label.c.cs_transfer_case_error'));
            }
        });
    },

    showError : function(component, event, helper, message) {
        var throwToastAction = $A.get('e.force:showToast');
        throwToastAction.setParams({
            'message': message,
            'type': 'error'
        });
        throwToastAction.fire();
    },

    checkOwnership : function(component, event, helper) {
         return new Promise($A.getCallback(function(resolve, reject) {
             var action = component.get('c.checkOwnership');
             var caseId = component.get('v.recordId');
             action.setParams({
                 'recordId' : caseId
             });
             action.setCallback(this, function(response) {
                 var state = response.getState();
                 if (state === 'SUCCESS') {
                     var response = response.getReturnValue();
                     if(response.isSuccess) {
                         if(response.isValid) {
                             resolve();
                         }
                         else {
                             reject($A.get('$Label.c.cs_ownership_error'));
                         }
                     }
                     else {
                         reject($A.get('$Label.c.cs_accept_case_error'));
                     }
                 }
                 else {
                     reject($A.get('$Label.c.cs_accept_case_error'));
                 }
             });
             $A.enqueueAction(action);
        })) ;
    },

    accept : function(component, event, helper) {
        var self = this;
         return new Promise($A.getCallback(function(resolve, reject) {
             var action = component.get('c.acceptCase');
             var caseId = component.get('v.recordId');
             var cases = [];
             var currentCase = {};
             currentCase.Id = caseId;
             currentCase.sobjectType = 'Case';
             cases.push(currentCase);
             action.setParams({
                 'caseJson' : JSON.stringify(cases)
             });
             action.setCallback(this, function(response) {
                 var state = response.getState();
                 if (state === 'SUCCESS') {
                     var response = response.getReturnValue();
                     if(response.isSuccess) {
                         self.init(component, event);
                         resolve();
                     }
                     else {
                         reject($A.get('$Label.c.cs_accept_case_error'));
                     }
                 }
                 else {
                     reject($A.get('$Label.c.cs_accept_case_error'));
                 }
             });
             $A.enqueueAction(action);
        })) ;
    },

    getCustomerType : function(component, event, helper) {
        return new Promise($A.getCallback(function(resolve, reject) {
             var action = component.get('c.getCustomerTypes');
             var caseId = component.get('v.recordId');
             action.setParams({
                 'caseId' : caseId
             });
             action.setCallback(this, function(response) {
                 var state = response.getState();
                 if (state === 'SUCCESS') {
                     var response = response.getReturnValue();
                     console.log('response: ' , response);
                     component.set("v.customerType", response.CustomerType__c);
                     component.set("v.customerTypeBannedSuspended", response.CustomerTypeBannedSuspended__c);
                     resolve();
                 }
                 else {
                     console.log('else 2');
                     reject($A.get('$Label.c.cs_get_fields_error'));
                 }
             });
             $A.enqueueAction(action);
        })) ;
    }
})