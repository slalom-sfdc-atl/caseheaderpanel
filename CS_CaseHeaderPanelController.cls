public class CS_CaseHeaderPanelController {
/**
* @description This class handles the custom header component (currently used on the case detail page).
* Logic is built off of a fieldset and a custom fieldset wrapper list to iterate across on the front end.
*/

/**
* @description Creates a list of FieldSetMembers to iterate across on front end.
* @param  sObjectName API name of sObject we are using
* @param fieldSetName API name of the field set we are using to display a set of fields
* @param id id of the record we are on (based on recordId)
* @return List<FieldSetMember> List of all FieldSetMembers we have generated
*/
    @AuraEnabled
    public static List<FieldSetMember> getFields(String sObjectName, String fieldSetName, String id) {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(sObjectName);
        Schema.DescribeSObjectResult describe = targetType.getDescribe();
        Map<String, Schema.FieldSet> fieldSetMap = describe.fieldSets.getMap();
        Schema.FieldSet fieldSet = fieldSetMap.get(fieldSetName);
        if(fieldSet != null) {
            List<Schema.FieldSetMember> fieldSetMembers = fieldSet.getFields();
            List<FieldSetMember> fieldSetMemberWrappers = new List<FieldSetMember>();
            for (Schema.FieldSetMember fieldSetMember: fieldSetMembers) {
                fieldSetMemberWrappers.add(new FieldSetMember(fieldSetMember, ''));
            }

            if(!String.isBlank(id)) {
                sObject sObjectWithFields = getsObjectWithFields(sObjectName, fieldSetMemberWrappers, id);
                for(FieldSetMember fieldSetMember : fieldSetMemberWrappers) {
                    fieldSetMember.value = recursiveGet(sObjectWithFields, fieldSetMember.fieldPath);
                }
            }
            System.debug('###fieldSetMemberWrappers: ' + fieldSetMemberWrappers);
            return fieldSetMemberWrappers;
        }
        else {
            return new List<FieldSetMember>();
        }
    }

    @AuraEnabled
    public static Case getCustomerTypes(Id caseId) {
        System.debug('###customerTypes: ' + [SELECT Id, CustomerType__c, CustomerTypeBannedSuspended__c FROM Case WHERE Id =: caseId]);
        return [SELECT Id, CustomerType__c, CustomerTypeBannedSuspended__c FROM Case WHERE Id =: caseId];
    }

/**
* @description Changes owner to the last queue the case was in based on QueueID__c
* @param  List<String> caseIds list of CaseIds to return to queue
*/
    @AuraEnabled
    public static void returnCaseToQueue(List<String> caseIds) {

        List<Case> currCases = [SELECT Id, Group__c, QueueName__c, QueueID__c, Status FROM Case WHERE Id IN: caseIds];
        List<Case> casesToUpdate = new List<Case>();
        for(Case currCase : currCases) {
            if(currCase.QueueID__c != null) {
                currCase.OwnerId = currCase.QueueID__c;
                if(currCase.Status == 'Open') {
                    currCase.Status = 'New';
                }
                casesToUpdate.add(currCase);
            }
        }
        try{
            update casesToUpdate;
        }catch(DMLException e) {
            throw new AuraHandledException(e.getMessage());
        }

    }

/**
* @description FieldSetMember wrapper class.  Used to recreate the FieldSetMember which is unavailable in lightning components.
* Added a value attribute so we can store the value of each field rather than having to key into another map
*/
    @TestVisible
    public with sharing class FieldSetMember {

        public FieldSetMember(Schema.FieldSetMember f, String value) {
            this.DBRequired = f.DBRequired;
            this.fieldPath = f.fieldPath;
            this.label = f.label;
            this.required = f.required;
            this.type = '' + f.getType();
            this.value = value;

        }

        public FieldSetMember(Boolean DBRequired) {
            this.DBRequired = DBRequired;
        }

        @AuraEnabled
        public Boolean DBRequired { get;set; }

        @AuraEnabled
        public String fieldPath { get;set; }

        @AuraEnabled
        public String label { get;set; }

        @AuraEnabled
        public Boolean required { get;set; }

        @AuraEnabled
        public String type { get; set; }

        @AuraEnabled
        public String value { get; set; }

        @AuraEnabled
        public Boolean show { get; set; }
    }

/**
* @description Parses fields with lookups so the correct value is returned
* @param  record sObject we are parsing
* @param field field API name we are parsing
* @return String value at the lookup field on the record
*/
	public static String recursiveGet(sObject record, String field) {
        if(field.contains('.')) {
            Integer firstPeriod = field.indexOf('.');
            String nextObjectName = field.subString(0, firstPeriod);
            String remainingfieldName = field.subString(firstPeriod + 1, field.length());
            sObject nextObject = record.getSObject(nextObjectName);
            if(nextObject == null) {
                return null;
            } else {
                return recursiveGet(nextObject, remainingfieldName);
            }
        } else {
            return String.valueOf(record.get(field));
        }
    }

/**
* @description Gets the values of the fields in the fieldset for a specific record
* @param  sObjectName API name of sObject we are parsing
* @param fset List<FieldSetMember> wrappers that we have populated
* @return id record Id we are displaying fields for
*/
	@AuraEnabled
    public static sObject getsObjectWithFields(String sObjectName, List<FieldSetMember> fset, Id id) {
        String query = 'SELECT ';
        for(FieldSetMember f : fset) {
            if(f.fieldPath.toLowerCase() != 'id')
                query += f.fieldPath + ', ';
        }
        query += 'Id FROM ' + sObjectName + ' WHERE Id =: id ';

        return Database.query(query);
    }

/**
* @description Gets the RecordType based on the recordTypeId
* @param  sObjectName API name of sObject we are parsing
* @param recordTypeId recordTypeId from sObject we are parsing.
* @return RecordType queried RecordType with additional fields
*/
	@AuraEnabled
    public static RecordType getRecordType(Id recordTypeId) {
        return [SELECT Id, Name, DeveloperName FROM RecordType WHERE Id =: recordTypeId];
    }

    @AuraEnabled
    public static ResponseModel checkOwnership(Id recordId) {
        Boolean isValid = false;

        UserRecordAccess access = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId =:UserInfo.getUserId() AND RecordId =: recordId];
        if(access.HasEditAccess) {
            isValid = true;
        }

        ResponseModel response = new ResponseModel();
        response.isSuccess = true;
        response.isValid = isValid;
        return response;
    }

    @AuraEnabled
    public static CS_SelectionAcceptController.ResponseModel acceptCase(String caseJson) {
        return CS_SelectionAcceptController.acceptCases(caseJson);
    }

    public class ResponseModel {
        @AuraEnabled public Boolean isSuccess;
        @AuraEnabled public String message;
        @AuraEnabled public Boolean isValid;
    }
}