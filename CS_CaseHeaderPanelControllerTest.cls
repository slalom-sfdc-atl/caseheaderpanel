@isTest
public class CS_CaseHeaderPanelControllerTest {
    static testmethod void getFieldsTest() {
        Case testCase = new Case();
        testCase.Status = 'Pending';
        testCase.Origin = 'Email';
        testCase.Subject = 'Test Subject';
        insert testCase;

        Test.startTest();
        	List<CS_CaseHeaderPanelController.FieldSetMember> fsm = CS_CaseHeaderPanelController.getFields('Case', 'Case_Header', testCase.Id);
        Test.stopTest();

        System.assertEquals(10, fsm.size(), 'We expect our fields to be returned.');
    }

    static testmethod void recursiveGetTest() {
        Account testAccount = new Account();
        testAccount.Name = 'Test';
        insert testAccount;

        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Reno';
        con.AccountId = testAccount.Id;
        insert con;

        Case childCase = new Case();
        childCase.Status = 'Pending';
        childCase.Origin = 'Email';
        childCase.Subject = 'Test Subject';
        childCase.ContactId = con.Id;
        insert childCase;

        Test.startTest();
        	String actual = CS_CaseHeaderPanelController.recursiveGet(childCase, 'Contact.Account');
        Test.stopTest();

        System.assertEquals(null, actual, 'We expect nothing to be returned.');
    }

    static testMethod void returnCaseToQueueTest() {
        List<Case> casesToInsert = new List<Case>();
        List<Id> caseIds = new List<Id>();
        List<QueueSobject> queues = new List<QueueSobject>();
        List<Group> groups = new List<Group>();
        QueueSobject testQueue1 = new QueueSobject();
        Group testGroup1 = new Group();
        User admin = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND IsActive = true LIMIT 1];
        System.runAs(admin) {

            testGroup1.Name = 'Test Queue 1';
            testGroup1.Type = 'Queue';
            groups.add(testGroup1);

            insert groups;

            testQueue1.QueueId = testGroup1.Id;
            testQueue1.SobjectType = 'Case';
            queues.add(testQueue1);

            insert queues;

            Case testCase1 = new Case();
            testCase1.Origin = 'Phone';
            testCase1.QueueID__c = testGroup1.Id;
            casesToInsert.add(testCase1);

            Case testCase2 = new Case();
            testCase2.Origin = 'Phone';
            testCase2.QueueID__c = testGroup1.Id;
            casesToInsert.add(testCase2);

            insert casesToInsert;
            caseIds.add(testCase1.Id);
            caseIds.add(testCase2.Id);
        }

        Test.startTest();
            CS_CaseHeaderPanelController.returnCaseToQueue(caseIds);
        Test.stopTest();

        List<Case> actualCases = [SELECT Id, OwnerId FROM Case WHERE Id IN: caseIds];

        System.assertEquals(actualCases[0].OwnerId, testGroup1.Id, 'We expect the owner to be the queue');
    }
}